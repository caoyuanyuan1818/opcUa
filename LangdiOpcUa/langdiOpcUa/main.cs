﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.Xml;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using LibUA;
using LibUA.Core;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace langdiOpcUa
{

    public partial class main : Form
    {
        ApplicationDescription appDesc = new ApplicationDescription(
              "urn:DemoApplication", "uri:DemoApplication", new LocalizedText("UA SDK client"),
              ApplicationType.Client, null, null, null);

    //    DemoClient client;
        private Timer timerUpdate;
        private DataTable dataTable;
        private NodeId selectedNodeId;
        private OpcUaClientManager ldOpcClient = new OpcUaClientManager();
        public main()
        {
            InitializeComponent();
            
            timerUpdate = new Timer();
            timerUpdate.Interval = 1000; 
            timerUpdate.Tick += TimerUpdate_Tick;
           
            dataTable = new DataTable();
            dataTable.Columns.Add("Node Name", typeof(string)); 
            dataTable.Columns.Add("Value", typeof(object));   
            dataGridViewValues.DataSource = dataTable;          
            dataGridViewValues.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGridView2.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
        }

        private void main_Load(object sender, EventArgs e)
        {
            connect conn = new connect();
            if (conn.ShowDialog() != DialogResult.OK)
            {
                return;
            }
            
            string opcServer = conn.OpcServerUrlText;
            string ipAddress = conn.OpcServerIpAddress;
            int port = conn.OpcServerPort;

            if(ldOpcClient.Connect(ipAddress, port, opcServer) != StatusCode.Good)
            {
                return;
            }

            BrowseResult[] childrenBrowseResults;
            childrenBrowseResults = ldOpcClient.Browse(new NodeId(0, (uint)UAConst.ObjectsFolder));

            if(childrenBrowseResults == null)
            {
                return;
            }
            foreach (var reference in childrenBrowseResults[0].Refs)
            {
                if(reference.BrowseName.Name == "FolderType")
                {
                    continue;
                }
                TreeNode treeNode = treeView1.Nodes.Add(reference.DisplayName.Text);
                treeNode.Tag = reference;
                BrowseSubNode(treeNode, reference);
            }
        }

        private void BrowseSubNode(TreeNode node, ReferenceDescription reference)
        {
            BrowseResult[] childrenBrowseResults;
            NodeId id = ((ReferenceDescription)node.Tag).TargetId;
            childrenBrowseResults = ldOpcClient.Browse(id);
            if (childrenBrowseResults == null)
            {
                return;
            }

            if(node.Level == 2)
            {
                return;
            }

            foreach (var refer in childrenBrowseResults[0].Refs)
            {
                if (refer.BrowseName.Name == "FolderType")
                {
                    continue;
                }
                TreeNode treeNode = node.Nodes.Add(refer.DisplayName.Text);
                treeNode.Tag = refer;
                BrowseSubNode(treeNode, refer);  
            }
        }

        private void BrowseSubNode2(TreeNode node, ReferenceDescription reference)
        {
            BrowseResult[] childrenBrowseResults;
            NodeId id = ((ReferenceDescription)node.Tag).TargetId;
            childrenBrowseResults = ldOpcClient.Browse(id);
            if (childrenBrowseResults == null)
            {
                return;
            }
            foreach (var refer in childrenBrowseResults[0].Refs)
            {
                if (refer.BrowseName.Name == "FolderType")
                {
                    continue;
                }
                TreeNode treeNode = node.Nodes.Add(refer.DisplayName.Text);
                treeNode.Tag = refer;
            }
        }
        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            ReferenceDescription reference = (ReferenceDescription)treeView1.SelectedNode.Tag;
            listBox1.Items.Clear();
            listBox1.Items.Add("DisplayName: " + reference.DisplayName.Text);
            listBox1.Items.Add("Locale     : " + reference.DisplayName.Locale);
            listBox1.Items.Add("DisplayName: " + reference.BrowseName.Name);
            listBox1.Items.Add("Namespace  : " + reference.BrowseName.NamespaceIndex.ToString());
            listBox1.Items.Add("IsForward  : " + reference.IsForward.ToString());
            listBox1.Items.Add("NodeClass  : " + reference.NodeClass.ToString());
            listBox1.Items.Add("TypeId     : " + reference.ReferenceTypeId.ToString());
            listBox1.Items.Add("TargetId   : " + reference.TargetId.ToString());
            listBox1.Items.Add("TypeDefine : " + reference.TypeDefinition.ToString());            
            TreeNode selectedNode = treeView1.SelectedNode;           
            selectedNodeId = ((ReferenceDescription)treeView1.SelectedNode.Tag).TargetId;           
            dataTable.Rows.Clear();           
            timerUpdate.Start();

            selectedNode.Nodes.Clear();
            BrowseSubNode2(selectedNode, reference);
        }

        private void TimerUpdate_Tick(object sender, EventArgs e)
        {
            if (selectedNodeId != null)
            {
                DataValue value = ldOpcClient.Read(selectedNodeId);
               if(value != null) {
                    string nodeName = treeView1.SelectedNode.Text;
                    object nodeValue = value.Value;
                    dataTable.Rows.Clear();
                    dataTable.Rows.Add(nodeName, nodeValue);
                }
            }
        }

        private void main_FormClosing(object sender, FormClosingEventArgs e)
        {           
            if (ldOpcClient != null)
            {
                timerUpdate.Stop();  
                chart1.Series.Clear();
                ldOpcClient.Disconnect();
            }
        }
        
        private void button_Click_ListWrite(object sender, EventArgs e)
        {
            if (ldOpcClient == null)
            {
                MessageBox.Show("Client is not initialized. Please connect to the server first.");
                return;
            }

            if (dataGridView2.Rows.Count == 0)
            {
                MessageBox.Show("No nodes selected for writing.");
                return;
            }

            List<WriteValue> writeValues = new List<WriteValue>();

            try
            {
                foreach (DataGridViewRow row in dataGridView2.Rows)
                {
                    if (row.Tag == null || row.Cells[1].Value == null)
                    {
                        continue;
                    }

                    NodeId nodeIdToWrite = ((ReferenceDescription)row.Tag).TargetId;
                    string writeValueStr = row.Cells[1].Value.ToString();

                    //Function "GetTypeAndWrite" are slowly than function "Write"
                    StatusCode ret = ldOpcClient.GetTypeAndWrite(nodeIdToWrite, writeValueStr);
                    if(ret == StatusCode.BadGeneric)
                    {
                        MessageBox.Show("NodeId or data format error!");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"An error occurred during writing: {ex.Message}");
            }
        }

        private void buttonClick_Close(object sender, EventArgs e)
        {
            try
            {
                timerUpdate.Stop();
                chart1.Series.Clear();
                if (ldOpcClient != null)
                {
                    ldOpcClient.Disconnect();
                    MessageBox.Show("Disconnected from the OPC server.");
                }
                else
                {
                    MessageBox.Show("No active connection to disconnect.");
                }
                Application.Exit();
            }
            catch (Exception ex)
            {
                
                MessageBox.Show($"An error occurred while closing the application: {ex.Message}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void treeView1_MouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if(e.Button == MouseButtons.Right)
            {               
                contextMenuStrip1.Show(treeView1, e.Location);
            }
        }

        private void addToReadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int rowIndex = dataGridView1.Rows.Add(treeView1.SelectedNode.Text);
            dataGridView1.Rows[rowIndex].Tag = treeView1.SelectedNode.Tag;
        }

        private void addToWriteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int rowIndex = dataGridView2.Rows.Add(treeView1.SelectedNode.Text);
            dataGridView2.Rows[rowIndex].Tag = treeView1.SelectedNode.Tag;
        }

        private void button2_Click_ListRead(object sender, EventArgs e)
        {
            try
            {
                NodeId[] id = new NodeId[dataGridView1.Rows.Count];
                for (int i = 0; i < dataGridView1.Rows.Count; i++)
                {
                    id[i] = ((ReferenceDescription)dataGridView1.Rows[i].Tag).TargetId;
                }
                DataValue[] dvs = ldOpcClient.Read(id);

                if(dvs == null)
                {
                    return;
                }

                for(int i = 0;i < dvs.Length;i++)
                {
                    if (dvs[i].Value == null)
                       continue;

                    dataGridView1.Rows[i].Cells[1].Value = dvs[i].Value.ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"An error occurred during reading: {ex.Message}");
            }
        }

        private void readHistoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                NodeId temp = ((ReferenceDescription)treeView1.SelectedNode.Tag).TargetId;

                if (temp == null)
                    return;

                DataValue[] dvs = ldOpcClient.ReadNodeHistory(
                    temp, 
                    new DateTime(2025, 2, 18),                
                    new DateTime(2025, 2, 19),               
                    100);

                if (dvs == null)
                    return;

                chart1.Series.Clear();
                chart1.Titles.Clear();

                chart1.Titles.Add(treeView1.SelectedNode.Text + " history value");
                chart1.ChartAreas[0].AxisX.Title = "points";
                chart1.ChartAreas[0].AxisY.Title = "value";
            
                chart1.ChartAreas[0].AxisX.ScaleView.Zoomable = true;
                chart1.ChartAreas[0].AxisY.ScaleView.Zoomable = true;
                chart1.ChartAreas[0].AxisX.ScrollBar.Enabled = true;
                chart1.ChartAreas[0].AxisY.ScrollBar.Enabled = true;
           
                Series series = new Series();
                series.ChartType = SeriesChartType.Line;          

                int maxPoint = dvs.Length;
                if (maxPoint > 0)
                maxPoint = 100;

                for (int i = 0; i < maxPoint; i++)
                {
                    series.Points.AddXY(i, dvs[i].Value); 
                }
     
                chart1.Series.Add(series);

            }
            catch (Exception ex)
            {
                MessageBox.Show($"Failed to read historical data: {ex.Message}");
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
 }

