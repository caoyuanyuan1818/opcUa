﻿using LibUA.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace langdiOpcUa
{
    public class OpcUaClientManager
    {
        private DemoClient client;   

        public OpcUaClientManager()
        {
            
        }

        public StatusCode Connect(string ipAddress, int port, string opcServer)
        {
            client = new DemoClient(ipAddress, port, 1000);
            
            StatusCode ret = client.Connect(opcServer);

            if (ret != StatusCode.Good)
            {
                MessageBox.Show("Connect to server error!");
                return ret;
            }
            client.OpenSecureChannel(MessageSecurityMode.None, SecurityPolicy.None, null);
            client.FindServers(out ApplicationDescription[] appDescs, new[] { "en" });
            client.GetEndpoints(out EndpointDescription[] endpointDescs, new[] { "en" });
            client.Disconnect();

            var messageSecurityMode = MessageSecurityMode.None;
            var securityPolicy = SecurityPolicy.None;
            bool useAnonymousUser = true;

            var endpointDesc = endpointDescs.First(e =>
                e.SecurityMode == messageSecurityMode &&
                e.SecurityPolicyUri == Types.SLSecurityPolicyUris[(int)securityPolicy]);
            byte[] serverCert = endpointDesc.ServerCertificate;

            client.Connect(opcServer);
            client.OpenSecureChannel(messageSecurityMode, securityPolicy, serverCert);
            client.CreateSession(new ApplicationDescription("urn:DemoApplication", "uri:DemoApplication", new LocalizedText("UA SDK client"), ApplicationType.Client, null, null, null), "urn:DemoApplication", 120);

            StatusCode activateRes;
            if (useAnonymousUser)
            {
                string policyId = endpointDesc.UserIdentityTokens.First(e => e.TokenType == UserTokenType.Anonymous).PolicyId;
                activateRes = client.ActivateSession(new UserIdentityAnonymousToken(policyId), new[] { "en" });
            }
            else
            {
                string policyId = endpointDesc.UserIdentityTokens.First(e => e.TokenType == UserTokenType.UserName).PolicyId;
                activateRes = client.ActivateSession(new UserIdentityUsernameToken(policyId, "plc-user", (new UTF8Encoding()).GetBytes("123"), Types.SignatureAlgorithmRsaOaep), new[] { "en" });
            }

            return StatusCode.Good;
        }

        public void Disconnect()
        {
            client.Disconnect();
        }

        public BrowseResult[] Browse(NodeId id)
        {
            StatusCode ret = client.Browse(new BrowseDescription[]
            {
                new BrowseDescription(id, BrowseDirection.Forward, NodeId.Zero, true, 0x0u, BrowseResultMask.All)
            }, 10000, out BrowseResult[] childrenBrowseResults);

            if(ret == StatusCode.Good)
            {
                return childrenBrowseResults;
            }
            else
            {
                return null;
            }
        }
        public DataValue[] Read(NodeId[] id)
        {
            ReadValueId[] ids = new ReadValueId[id.Length] ;

            for (int i = 0; i < id.Length ; i++)
            {
                ids[i] = new ReadValueId(id[i], NodeAttribute.Value, null, new QualifiedName(0, null));
            }

            var readRes = client.Read(ids, out DataValue[] dataValues);

            if(readRes == StatusCode.Good)
            {
                return dataValues;
            }
            else
            { return null; }
        }

        public DataValue Read(NodeId id)
        {
        
            ReadValueId  readId = new ReadValueId(id, NodeAttribute.Value, null, new QualifiedName(0, null));

            var readRes = client.Read(new ReadValueId[] {
                readId,
            }, out DataValue[] dataValues);

            if ((readRes == StatusCode.Good) && (dataValues.Length > 0))
            {
                return dataValues[0];
            }
            else
            { return null; }
        }

        public StatusCode GetTypeAndWrite(NodeId id, object value)
        {
            DataValue res = Read(id);
            if (res == null)
                return StatusCode.BadGeneric;

            try
            {
                object convertedValue;
                convertedValue = Convert.ChangeType(value, res.Value.GetType());
                return Write(id, convertedValue);
            }
            catch
            {
                return StatusCode.BadGeneric;
            } 
        }

        public StatusCode Write(NodeId id,object value)
        {
            WriteValue[] v = new WriteValue[1];
            v[0] = new WriteValue(
                id,
                NodeAttribute.Value,
                null,
                new DataValue(value, StatusCode.GoodClamped, DateTime.Now)
                );
            return client.Write(v, out uint[] respStatuses);
        }

        public StatusCode Write(NodeId[] id, object value)
        {
            WriteValue[] v = new WriteValue[id.Length];
            for (int i = 0; i < id.Length; i++)
            {
                v[i] = new WriteValue(
               id[i],
               NodeAttribute.Value,
               null,
               new DataValue(value, StatusCode.GoodClamped, DateTime.Now)
               );
            }
               
            return client.Write(v, out uint[] respStatuses);
        }

        public DataValue[] ReadNodeHistory(NodeId id,DateTime startTime,DateTime endTime,uint size)
        {
            ReadRawModifiedDetails details = new ReadRawModifiedDetails(
                false,
                startTime,
                endTime,
                size, true
                );

            client.HistoryRead(
                details, 
                TimestampsToReturn.Both, 
                false,
                new HistoryReadValueId[] {
                    new HistoryReadValueId(id, null, new QualifiedName(), null),
                }, out HistoryReadResult[] histResults);

            if(histResults.Length > 0 )
            {
                return histResults[0].Values;
            }
            else
            {
                return null;
            }
        }

        //public void BrowseAvailableNodes(Action<BrowseResult[]> callback)
        //{
        //    client.Browse(new BrowseDescription[]
        //    {
        //        new BrowseDescription(new NodeId(0, (uint)AConst.ObjectsFolder), BrowseDirection.Forward, NodeId.Zero, true, 0x0u, BrowseResultMask.All)
        //    }, 10000, out BrowseResult[] childrenBrowseResults);

        //    callback(childrenBrowseResults);
        //}

        //public void ReadObjectByNodeId(NodeId nodeId, Action<DataValue[]> callback)
        //{
        //    var readRes = client.Read(new ReadValueId[] {
        //        new ReadValueId(nodeId, NodeAttribute.Value, null, new QualifiedName(0, null)),
        //    }, out DataValue[] dataValues);

        //    callback(dataValues);
        //}

        //public void WriteObjectByNodeId(NodeId nodeId, string writeValueStr, Action<bool> callback)
        //{
        //    var readRes = client.Read(new ReadValueId[] {
        //        new ReadValueId(nodeId, NodeAttribute.Value, null, new QualifiedName(0, null)),
        //    }, out DataValue[] dataValues);

        //    if (dataValues.Length == 0)
        //    {
        //        callback(false);
        //        return;
        //    }

        //    var nodeValue = dataValues[0].Value;
        //    Type nodeType = nodeValue.GetType();

        //    object convertedValue;
        //    try
        //    {
        //        convertedValue = Convert.ChangeType(writeValueStr, nodeType);
        //    }
        //    catch (Exception)
        //    {
        //        callback(false);
        //        return;
        //    }

        //    var writeValues = new List<WriteValue>
        //    {
        //        new WriteValue(nodeId, NodeAttribute.Value, null, new DataValue(convertedValue, StatusCode.GoodClamped, DateTime.Now))
        //    };

        //    client.Write(writeValues.ToArray(), out uint[] respStatuses);
        //    bool success = respStatuses.All(status => status == (uint)StatusCode.Good);
        //    callback(success);
        //}

        //public void ReadNodeHistory(NodeId nodeId, Action<HistoryReadResult[]> callback)
        //{
        //    client.HistoryRead(new ReadRawModifiedDetails(false,
        //        new DateTime(2025, 2, 18),
        //        new DateTime(2025, 2, 19),
        //        100, true), TimestampsToReturn.Both, false,
        //        new HistoryReadValueId[] {
        //            new HistoryReadValueId(nodeId, null, new QualifiedName(), null),
        //        }, out HistoryReadResult[] histResults);

        //    callback(histResults);
        //}
    }
}
