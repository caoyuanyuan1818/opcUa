﻿using LibUA.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LibUA;

namespace langdiOpcUa
{
    public partial class connect : Form
    {
        public connect()
        {
            InitializeComponent();
        }

        public string OpcServerUrlText
        {
            get { return textBox1.Text; }
        }

        public string OpcServerIpAddress
        {
            get { return textBox3.Text; }
        }

        public int OpcServerPort
        { 
            get {
                int port = 0;
                try
                {
                    port = int.Parse(textBox4.Text);
                }
                catch
                {
                    port = 53530;
                }
                return port; 
            }
        }

        public string OpcSessionName
        {
            get { return textBox2.Text; }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }


    }
}


      